use std::convert::TryInto;

use fnv::FnvHashMap;

use crate::{
    data::{
        Class, Constant, ConstantPoolIndex, ConstantTag, LocalIndex, Method, Opcode, OpcodeTag,
    },
    fml::TopLevelParser,
    parser::AST,
};

const LAMBDA: &str = "λ:";

#[derive(Debug, Clone)]
struct FunctionData {
    name: String,
    code: Vec<Opcode>,
    num_arguments: u8,
    num_locals: u16,
    scopes: Vec<FnvHashMap<String, LocalIndex>>,
}

impl FunctionData {
    fn new(name: String, num_arguments: u8) -> Self {
        Self {
            name,
            code: Vec::new(),
            num_arguments,
            num_locals: 0,
            scopes: Vec::new(),
        }
    }
}

#[derive(Debug, Clone)]
struct Program {
    const_pool: Vec<Constant>,
    globals: Vec<ConstantPoolIndex>,
    entry_point: Option<ConstantPoolIndex>,
    all_code: Vec<Opcode>,

    const_indices: FnvHashMap<Constant, usize>,
    current_fn: FunctionData,
    fn_stack: Vec<FunctionData>,

    label_counter: usize,
    array_counter: usize,
}

impl Program {
    fn new() -> Self {
        Self {
            const_pool: Vec::new(),
            globals: Vec::new(),
            entry_point: None,
            all_code: Vec::new(),

            const_indices: FnvHashMap::default(),
            current_fn: FunctionData::new(LAMBDA.to_owned(), 0),
            fn_stack: Vec::new(),

            label_counter: 0,
            array_counter: 0,
        }
    }

    fn register_constant(&mut self, constant: Constant) -> ConstantPoolIndex {
        let const_pool = &mut self.const_pool; // borrowchk dance
        let index = *self
            .const_indices
            .entry(constant.clone())
            .or_insert_with(|| {
                let index = const_pool.len();
                const_pool.push(constant);
                index
            });
        ConstantPoolIndex(index.try_into().unwrap())
    }

    fn compile_constant(&mut self, constant: Constant) -> bool {
        let const_index = self.register_constant(constant);
        self.current_fn.code.push(Opcode::Literal { const_index });
        true
    }

    fn compile_node(&mut self, ast: &AST) -> bool {
        match ast {
            AST::Integer(value) => self.compile_constant(Constant::Integer(*value)),
            AST::Boolean(value) => self.compile_constant(Constant::Boolean(*value)),
            AST::Null => self.compile_constant(Constant::Null),
            AST::Variable { name, value } => {
                let element_on_stack = self.compile_node(value);
                assert!(element_on_stack);

                if self.current_fn.scopes.is_empty() {
                    // New global
                    let name_index = self.register_constant(Constant::String(name.0.clone()));
                    let slot_index = self.register_constant(Constant::Slot(name_index));
                    self.globals.push(slot_index);
                    self.current_fn.code.push(Opcode::SetGlobal { name_index });
                } else {
                    // New local
                    let num_args = u16::from(self.current_fn.num_arguments);
                    let local_index = LocalIndex(num_args + self.current_fn.num_locals);
                    self.current_fn.code.push(Opcode::SetLocal { local_index });

                    let top_scope = self.current_fn.scopes.last_mut().unwrap();
                    top_scope.insert(name.0.clone(), local_index);
                    self.current_fn.num_locals += 1;
                }

                element_on_stack
            }
            AST::Array { size, value } => {
                match &**value {
                    AST::Null | AST::Integer(_) | AST::Boolean(_) => {
                        // Base case - no init loop

                        let size_on_stack = self.compile_node(size);
                        assert!(size_on_stack);

                        let value_on_stack = self.compile_node(value);
                        assert!(value_on_stack);

                        self.current_fn.code.push(Opcode::Array);
                        true
                    }
                    _ => {
                        // Cases where we potentially need to geenrate a loop after creating the array to initialize it

                        // Create names and increment immediately in case there's some weird nesting
                        // (e.g. the initializer creates another array)
                        let name_size = format!("::size{}", self.array_counter);
                        let name_array = format!("::array{}", self.array_counter);
                        let name_i = format!("::i{}", self.array_counter);
                        self.array_counter += 1;

                        // Save the size to ::size for later
                        let size_on_stack = self.compile_node(&AST::variable(
                            name_size.clone().into(),
                            (**size).clone(),
                        ));
                        assert!(size_on_stack);
                        self.current_fn.code.push(Opcode::Drop);

                        // Create the array using the base case and save it to ::array
                        let ast_array = AST::array((**size).clone(), AST::null());
                        let ast_array_variable =
                            AST::variable(name_array.clone().into(), ast_array);
                        let array_on_stack = self.compile_node(&ast_array_variable);
                        assert!(array_on_stack);

                        // Init the ::i counter
                        let ast_i_variable = AST::variable(name_i.clone().into(), AST::integer(0));
                        let i_on_stack = self.compile_node(&ast_i_variable);
                        assert!(i_on_stack);
                        self.current_fn.code.push(Opcode::Drop);

                        // The init loop (using ::i and ::size)
                        let ast_loop_condition = AST::call_method(
                            AST::access_variable(name_i.clone().into()),
                            "<".into(),
                            vec![AST::access_variable(name_size.clone().into())],
                        );
                        let ast_loop_body = AST::block(vec![
                            AST::assign_array(
                                AST::access_variable(name_array.clone().into()),
                                AST::access_variable(name_i.clone().into()),
                                (**value).clone(),
                            ),
                            AST::assign_variable(
                                name_i.clone().into(),
                                AST::call_method(
                                    AST::access_variable(name_i.clone().into()),
                                    "+".into(),
                                    vec![AST::integer(1)],
                                ),
                            ),
                        ]);
                        let ast_loop = AST::loop_de_loop(ast_loop_condition, ast_loop_body);
                        let loop_result_on_stack = self.compile_node(&ast_loop);
                        assert!(loop_result_on_stack);
                        self.current_fn.code.push(Opcode::Drop);

                        // Everything after creating the array was dropped so the array reference is on the stack
                        array_on_stack
                    }
                }
            }
            AST::Object { extends, members } => {
                let parent_on_stack = self.compile_node(extends);
                assert!(parent_on_stack);

                let mut class_members = Vec::new();
                for member in members {
                    match &**member {
                        AST::Variable { name, value } => {
                            let value_on_stack = self.compile_node(value);
                            assert!(value_on_stack);
                            let name_index =
                                self.register_constant(Constant::String(name.0.clone()));
                            let slot_index = self.register_constant(Constant::Slot(name_index));
                            class_members.push(slot_index);
                        }
                        AST::Function { .. } => {
                            let method_index = self.compile_function_or_method(member, true);
                            class_members.push(method_index);
                        }
                        _ => unreachable!("Objects can only have fields and methods"),
                    }
                }

                let class_index = self.register_constant(Constant::Class(Class {
                    members: class_members,
                }));

                self.current_fn.code.push(Opcode::Object { class_index });
                true
            }
            AST::AccessVariable { name } => {
                let mut local = false;
                for scope in self.current_fn.scopes.iter() {
                    if scope.contains_key(name.as_str()) {
                        // Get local
                        let local_index = *scope.get(name.as_str()).unwrap();
                        self.current_fn.code.push(Opcode::GetLocal { local_index });

                        local = true;
                        break;
                    }
                }

                if !local {
                    // Get global
                    let name_index = self.register_constant(Constant::String(name.0.clone()));
                    self.current_fn.code.push(Opcode::GetGlobal { name_index });
                }

                true
            }
            AST::AccessField { object, field } => {
                let receiver_on_stack = self.compile_node(object);
                assert!(receiver_on_stack);

                let name_index = self.register_constant(Constant::String(field.0.clone()));

                self.current_fn.code.push(Opcode::GetField { name_index });

                true
            }
            AST::AccessArray { array, index } => self.compile_node(&AST::call_method(
                (**array).clone(),
                "get".into(),
                vec![(**index).clone()],
            )),
            AST::AssignVariable { name, value } => {
                self.compile_node(value);

                let mut local = false;
                for scope in self.current_fn.scopes.iter() {
                    if scope.contains_key(name.as_str()) {
                        // Assign local
                        let local_index = *scope.get(name.as_str()).unwrap();
                        self.current_fn.code.push(Opcode::SetLocal { local_index });

                        local = true;
                        break;
                    }
                }

                if !local {
                    // Assign global
                    let name_index = self.register_constant(Constant::String(name.0.clone()));
                    self.current_fn.code.push(Opcode::SetGlobal { name_index });
                }

                true
            }
            AST::AssignField {
                object,
                field,
                value,
            } => {
                let receiver_on_stack = self.compile_node(object);
                assert!(receiver_on_stack);

                let value_on_stack = self.compile_node(value);
                assert!(value_on_stack);

                let name_index = self.register_constant(Constant::String(field.0.clone()));

                self.current_fn.code.push(Opcode::SetField { name_index });

                value_on_stack
            }
            AST::AssignArray {
                array,
                index,
                value,
            } => self.compile_node(&AST::call_method(
                (**array).clone(),
                "set".into(),
                vec![(**index).clone(), (**value).clone()],
            )),
            AST::Function { .. } => {
                // No nested function definitions
                assert_eq!(self.current_fn.name, LAMBDA);
                assert_eq!(self.current_fn.scopes.len(), 0);

                let method_index = self.compile_function_or_method(ast, false);
                self.globals.push(method_index);

                false
            }
            AST::CallFunction { name, arguments } => {
                for arg in arguments {
                    let argument_on_stack = self.compile_node(arg);
                    assert!(argument_on_stack);
                }

                let name_index = self.register_constant(Constant::String(name.0.clone()));
                let num_args = arguments.len().try_into().unwrap();
                self.current_fn.code.push(Opcode::CallFunction {
                    name_index,
                    num_args,
                });

                true
            }
            AST::CallMethod {
                object,
                name,
                arguments,
            } => {
                let receiver_on_stack = self.compile_node(object);
                assert!(receiver_on_stack);

                for arg in arguments {
                    let argument_on_stack = self.compile_node(arg);
                    assert!(argument_on_stack);
                }

                let name_index = self.register_constant(Constant::String(name.0.clone()));
                let num_args: u8 = arguments.len().try_into().unwrap();
                self.current_fn.code.push(Opcode::CallMethod {
                    name_index,
                    num_args: num_args + 1, // The +1 is the receiver
                });

                true
            }
            AST::Top(nodes) => {
                // Put this first like the reference compiler does
                self.register_constant(Constant::String(LAMBDA.to_owned()));

                // Compile the whole program
                let mut element_on_stack = false;
                for node in nodes {
                    if element_on_stack {
                        self.current_fn.code.push(Opcode::Drop);
                    }
                    element_on_stack = self.compile_node(node);
                }

                // The implicit global function (and everything else too) is complete now
                assert_eq!(self.current_fn.name, LAMBDA);
                assert_eq!(self.current_fn.scopes.len(), 0);
                assert!(self.fn_stack.is_empty());
                let length = self.current_fn.code.len().try_into().unwrap();
                let num_locals = self.current_fn.num_locals;
                let start = self.all_code.len().try_into().unwrap();
                let method = Method {
                    name_index: ConstantPoolIndex(0),
                    num_arguments: 0,
                    num_locals,
                    start,
                    length,
                };
                self.all_code.extend(self.current_fn.code.iter());

                // Push the implicit global function and get its index.
                // Note: unlike normal functions, it's not added to globals.
                let method_index = self.register_constant(Constant::Method(method));

                // Use the index as entry point
                assert_eq!(self.entry_point, None);
                self.entry_point = Some(method_index);

                element_on_stack
            }
            AST::Block(nodes) => {
                assert!(!nodes.is_empty()); // Empty block is parsed as AST::Null so there's always at least one node (also see element_on_stack)

                self.current_fn.scopes.push(FnvHashMap::default());

                let mut element_on_stack = false;
                for node in nodes {
                    if element_on_stack {
                        self.current_fn.code.push(Opcode::Drop);
                    }
                    element_on_stack = self.compile_node(node);
                    // FML doesn't allow declaring functions inside blocks so whatever the node is,
                    // it has to put something on the stack.
                    assert!(element_on_stack);
                }

                self.current_fn.scopes.pop();

                assert!(element_on_stack); // This is the logical result of the previous two asserts.
                element_on_stack
            }
            AST::Loop { condition, body } => {
                // Prepare labels
                let index_body = self.register_constant(Constant::String(format!(
                    "loop:body:{}",
                    self.label_counter
                )));
                let index_condition = self.register_constant(Constant::String(format!(
                    "loop:condition:{}",
                    self.label_counter
                )));
                self.label_counter += 1;

                // Jump to condition
                self.current_fn.code.push(Opcode::Jump {
                    label_name_index: index_condition,
                });

                // Body
                self.current_fn.code.push(Opcode::Label {
                    label_name_index: index_body,
                });
                let body_on_stack = self.compile_node(body);
                assert!(body_on_stack);
                self.current_fn.code.push(Opcode::Drop);

                // Condition
                self.current_fn.code.push(Opcode::Label {
                    label_name_index: index_condition,
                });
                let condition_on_stack = self.compile_node(condition);
                assert!(condition_on_stack);
                self.current_fn.code.push(Opcode::Branch {
                    label_name_index: index_body,
                });

                // All loops always return null
                self.compile_constant(Constant::Null)
            }
            AST::Conditional {
                condition,
                consequent,
                alternative,
            } => {
                // Prepare labels
                let index_consequent = self.register_constant(Constant::String(format!(
                    "if:consequent:{}",
                    self.label_counter
                )));
                let index_end = self
                    .register_constant(Constant::String(format!("if:end:{}", self.label_counter)));
                self.label_counter += 1;

                // Condition
                let condition_on_stack = self.compile_node(condition);
                assert!(condition_on_stack);

                self.current_fn.code.push(Opcode::Branch {
                    label_name_index: index_consequent,
                });

                // Alternative
                let alternative_on_stack = self.compile_node(alternative);
                assert!(alternative_on_stack);

                self.current_fn.code.push(Opcode::Jump {
                    label_name_index: index_end,
                });

                // Consequent
                self.current_fn.code.push(Opcode::Label {
                    label_name_index: index_consequent,
                });
                let consequent_on_stack = self.compile_node(consequent);
                assert!(consequent_on_stack);

                self.current_fn.code.push(Opcode::Label {
                    label_name_index: index_end,
                });

                // Both branches always exist (a missing else in code will produce AST::Null)
                // so we always push something on the stack no matter which branch we take.
                // If that value is not used, the parent node will drop it
                // (we don't even know here if the value is used) - the drop will be after the "if:end:X" label".
                // This is different from the reference compiler - if the value is not used, it drops it in each branch
                // (so it likely passes down information whether it's used or not into child nodes).
                true
            }
            AST::Print { format, arguments } => {
                for arg in arguments {
                    self.compile_node(arg);
                }
                let fmt_str_index = self.register_constant(Constant::String(format.clone()));
                let num_args = arguments.len().try_into().unwrap();
                self.current_fn.code.push(Opcode::Print {
                    fmt_str_index,
                    num_args,
                });

                true
            }
        }
    }

    fn compile_function_or_method(&mut self, ast: &AST, is_method: bool) -> ConstantPoolIndex {
        if let AST::Function {
            name,
            parameters,
            body,
        } = ast
        {
            let mut num_arguments = parameters.len().try_into().unwrap();
            if is_method {
                num_arguments += 1;
            }
            let new_fn = FunctionData::new(name.0.clone(), num_arguments);
            let parent_fn = std::mem::replace(&mut self.current_fn, new_fn);
            self.fn_stack.push(parent_fn);

            let mut args_scope = FnvHashMap::default();
            let mut arg_index = 0;
            if is_method {
                args_scope.insert("this".to_owned(), LocalIndex(arg_index));
                arg_index += 1;
            }
            for param_name in parameters {
                args_scope.insert(param_name.0.clone(), LocalIndex(arg_index));
                arg_index += 1;
            }
            self.current_fn.scopes.push(args_scope);

            let return_on_stack = self.compile_node(body);
            assert!(return_on_stack); // Not actually on stack now but calling the function will put it there
            self.current_fn.code.push(Opcode::Return);

            self.current_fn.scopes.pop();

            let name_index = self.register_constant(Constant::String(name.0.clone()));
            let num_locals = self.current_fn.num_locals;
            let start = self.all_code.len().try_into().unwrap();
            let length = self.current_fn.code.len().try_into().unwrap();
            let method = Method {
                name_index,
                num_arguments,
                num_locals,
                start,
                length,
            };
            self.all_code.extend(self.current_fn.code.iter());
            self.current_fn = self.fn_stack.pop().unwrap();

            let method_index = self.register_constant(Constant::Method(method));
            method_index
        } else {
            unreachable!("Expected AST::Function")
        }
    }

    #[cfg(feature = "mydbg")]
    fn eprint(&self) {
        eprintln!("Constant Pool:");
        for (i, constant) in self.const_pool.iter().enumerate() {
            eprint!("{}: ", i);
            match constant {
                Constant::Integer(value) => eprintln!("{}", value),
                Constant::Boolean(value) => eprintln!("{}", value),
                Constant::Null => eprintln!("null"),
                Constant::String(s) => eprintln!("\"{}\"", s),
                Constant::Slot(index) => eprintln!("slot #{}", index.0),
                Constant::Method(method) => eprintln!(
                    "method #{} args:{} locals:{} {:04}-{:04}",
                    method.name_index.0,
                    method.num_arguments,
                    method.num_locals,
                    method.start,
                    method.start + method.length - 1
                ),
                Constant::Class(class) => eprintln!(
                    "class {}",
                    class
                        .members
                        .iter()
                        .map(|index| format!("#{}", index.0))
                        .collect::<Vec<_>>()
                        .join(",")
                ),
            }
        }

        eprintln!("Entry: #{}", self.entry_point.unwrap().0);

        eprintln!("Globals:");
        for (i, index) in self.globals.iter().enumerate() {
            eprintln!("{}: #{}", i, index.0);
        }

        eprintln!("Code:");
        for (i, opcode) in self.all_code.iter().enumerate() {
            eprint!("{}: ", i);
            match opcode {
                Opcode::Label { label_name_index } => eprintln!("label #{}", label_name_index.0),
                Opcode::Literal { const_index } => eprintln!("lit #{}", const_index.0),
                Opcode::Print {
                    fmt_str_index,
                    num_args,
                } => eprintln!("printf #{} {}", fmt_str_index.0, num_args),
                Opcode::Array => eprintln!("array"),
                Opcode::Object { class_index } => eprintln!("object #{}", class_index.0),
                // These 2 are just printed as get/set slot but in reality they point at the actual string constant.
                // There's no indirection through slots.
                Opcode::GetField { name_index } => eprintln!("get slot #{}", name_index.0),
                Opcode::SetField { name_index } => eprintln!("set slot #{}", name_index.0),
                Opcode::CallMethod {
                    name_index,
                    num_args,
                } => {
                    // There's no slot involved, the reference just prints it this way for some reason
                    eprintln!("call slot #{} {}", name_index.0, num_args);
                }
                Opcode::CallFunction {
                    name_index,
                    num_args,
                } => eprintln!("call #{} {}", name_index.0, num_args),
                Opcode::SetLocal { local_index } => eprintln!("set local ::{}", local_index.0),
                Opcode::GetLocal { local_index } => eprintln!("get local ::{}", local_index.0),
                Opcode::SetGlobal { name_index } => eprintln!("set global #{}", name_index.0),
                Opcode::GetGlobal { name_index } => eprintln!("get global #{}", name_index.0),
                Opcode::Branch { label_name_index } => eprintln!("branch #{}", label_name_index.0),
                Opcode::Jump { label_name_index } => eprintln!("goto #{}", label_name_index.0),
                Opcode::Return => eprintln!("return"),
                Opcode::Drop => eprintln!("drop"),
            }
        }
    }

    fn generate_bytecode(&self) -> Vec<u8> {
        let mut bytecode = Vec::new();

        let const_pool_len: u16 = self.const_pool.len().try_into().unwrap();
        bytecode.extend_from_slice(&const_pool_len.to_le_bytes());
        for constant in &self.const_pool {
            match constant {
                Constant::Integer(value) => {
                    bytecode.push(ConstantTag::Integer.into());
                    bytecode.extend_from_slice(&value.to_le_bytes());
                }
                Constant::Boolean(value) => {
                    bytecode.push(ConstantTag::Boolean.into());
                    bytecode.push((*value).into());
                }
                Constant::Null => {
                    bytecode.push(ConstantTag::Null.into());
                }
                Constant::String(string) => {
                    bytecode.push(ConstantTag::String.into());
                    let length: u32 = string.len().try_into().unwrap();
                    bytecode.extend_from_slice(&length.to_le_bytes());
                    bytecode.extend_from_slice(string.as_bytes());
                }
                Constant::Slot(index) => {
                    bytecode.push(ConstantTag::Slot.into());
                    bytecode.extend_from_slice(&index.0.to_le_bytes());
                }
                Constant::Method(method) => {
                    bytecode.push(ConstantTag::Method.into());
                    bytecode.extend_from_slice(&method.name_index.0.to_le_bytes());
                    bytecode.push(method.num_arguments);
                    bytecode.extend_from_slice(&method.num_locals.to_le_bytes());
                    let num_opcodes: u32 = method.length.try_into().unwrap();
                    bytecode.extend_from_slice(&num_opcodes.to_le_bytes());

                    for i in method.start..(method.start + method.length) {
                        match self.all_code[i] {
                            Opcode::Label { label_name_index } => {
                                bytecode.push(OpcodeTag::Label.into());
                                bytecode.extend_from_slice(&label_name_index.0.to_le_bytes());
                            }
                            Opcode::Literal { const_index } => {
                                bytecode.push(OpcodeTag::Literal.into());
                                bytecode.extend_from_slice(&const_index.0.to_le_bytes());
                            }
                            Opcode::Print {
                                fmt_str_index,
                                num_args,
                            } => {
                                bytecode.push(OpcodeTag::Print.into());
                                bytecode.extend_from_slice(&fmt_str_index.0.to_le_bytes());
                                bytecode.push(num_args);
                            }
                            Opcode::Array => {
                                bytecode.push(OpcodeTag::Array.into());
                            }
                            Opcode::Object { class_index } => {
                                bytecode.push(OpcodeTag::Object.into());
                                bytecode.extend_from_slice(&class_index.0.to_le_bytes());
                            }
                            Opcode::GetField { name_index } => {
                                bytecode.push(OpcodeTag::GetField.into());
                                bytecode.extend_from_slice(&name_index.0.to_le_bytes());
                            }
                            Opcode::SetField { name_index } => {
                                bytecode.push(OpcodeTag::SetField.into());
                                bytecode.extend_from_slice(&name_index.0.to_le_bytes());
                            }
                            Opcode::CallMethod {
                                name_index,
                                num_args,
                            } => {
                                bytecode.push(OpcodeTag::CallMethod.into());
                                bytecode.extend_from_slice(&name_index.0.to_le_bytes());
                                bytecode.push(num_args);
                            }
                            Opcode::CallFunction {
                                name_index,
                                num_args,
                            } => {
                                bytecode.push(OpcodeTag::CallFunction.into());
                                bytecode.extend_from_slice(&name_index.0.to_le_bytes());
                                bytecode.push(num_args);
                            }
                            Opcode::SetLocal { local_index } => {
                                bytecode.push(OpcodeTag::SetLocal.into());
                                bytecode.extend_from_slice(&local_index.0.to_le_bytes());
                            }
                            Opcode::GetLocal { local_index } => {
                                bytecode.push(OpcodeTag::GetLocal.into());
                                bytecode.extend_from_slice(&local_index.0.to_le_bytes());
                            }
                            Opcode::SetGlobal { name_index } => {
                                bytecode.push(OpcodeTag::SetGlobal.into());
                                bytecode.extend_from_slice(&name_index.0.to_le_bytes());
                            }
                            Opcode::GetGlobal { name_index } => {
                                bytecode.push(OpcodeTag::GetGlobal.into());
                                bytecode.extend_from_slice(&name_index.0.to_le_bytes());
                            }
                            Opcode::Branch { label_name_index } => {
                                bytecode.push(OpcodeTag::Branch.into());
                                bytecode.extend_from_slice(&label_name_index.0.to_le_bytes());
                            }
                            Opcode::Jump { label_name_index } => {
                                bytecode.push(OpcodeTag::Jump.into());
                                bytecode.extend_from_slice(&label_name_index.0.to_le_bytes());
                            }
                            Opcode::Return => {
                                bytecode.push(OpcodeTag::Return.into());
                            }
                            Opcode::Drop => {
                                bytecode.push(OpcodeTag::Drop.into());
                            }
                        }
                    }
                }
                Constant::Class(class) => {
                    bytecode.push(ConstantTag::Class.into());
                    let length: u16 = class.members.len().try_into().unwrap();
                    bytecode.extend_from_slice(&length.to_le_bytes());
                    for member in &class.members {
                        bytecode.extend_from_slice(&member.0.to_le_bytes());
                    }
                }
            }
        }

        let globals_len: u16 = self.globals.len().try_into().unwrap();
        bytecode.extend_from_slice(&globals_len.to_le_bytes());
        for global in &self.globals {
            bytecode.extend_from_slice(&global.0.to_le_bytes());
        }

        bytecode.extend_from_slice(&self.entry_point.unwrap().0.to_le_bytes());

        bytecode
    }
}

pub fn compile(text: &str) -> Vec<u8> {
    let ast: AST = TopLevelParser::new().parse(&text).expect("Parse error");

    let mut program = Program::new();
    program.compile_node(&ast);

    #[cfg(feature = "mydbg")]
    program.eprint();

    program.generate_bytecode()
}
