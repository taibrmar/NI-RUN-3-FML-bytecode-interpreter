#[allow(unused)]
macro_rules! mydbg {
    ( $( $t:tt )* ) => {
        #[cfg(feature = "mydbg")]
        dbg!( $( $t )* );
    }
}
