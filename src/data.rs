use num_enum::{IntoPrimitive, TryFromPrimitive};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Constant {
    Integer(i32),
    Boolean(bool),
    Null,
    String(String),
    Slot(ConstantPoolIndex),
    Method(Method),
    Class(Class),
}

impl Constant {
    pub fn unwrap_str(&self) -> &str {
        if let Constant::String(string) = self {
            string
        } else {
            panic!("{:?} is not a string", self)
        }
    }

    pub fn unwrap_method(&self) -> &Method {
        if let Constant::Method(method) = self {
            method
        } else {
            panic!("{:?} is not a method", self)
        }
    }

    pub fn unwrap_class(&self) -> &Class {
        if let Constant::Class(class) = self {
            class
        } else {
            panic!("{:?} is not a class", self)
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Method {
    pub name_index: ConstantPoolIndex,
    pub num_arguments: u8,
    pub num_locals: u16,
    pub start: usize,
    pub length: usize,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Class {
    pub members: Vec<ConstantPoolIndex>,
}

#[derive(Debug, Clone, Copy)]
pub enum Opcode {
    Label {
        label_name_index: ConstantPoolIndex,
    },
    Literal {
        const_index: ConstantPoolIndex,
    },
    Print {
        fmt_str_index: ConstantPoolIndex,
        num_args: u8,
    },
    Array,
    Object {
        class_index: ConstantPoolIndex,
    },
    GetField {
        name_index: ConstantPoolIndex,
    },
    SetField {
        name_index: ConstantPoolIndex,
    },
    CallMethod {
        name_index: ConstantPoolIndex,
        num_args: u8,
    },
    CallFunction {
        name_index: ConstantPoolIndex,
        num_args: u8,
    },
    SetLocal {
        local_index: LocalIndex,
    },
    GetLocal {
        local_index: LocalIndex,
    },
    SetGlobal {
        name_index: ConstantPoolIndex,
    },
    GetGlobal {
        name_index: ConstantPoolIndex,
    },
    Branch {
        label_name_index: ConstantPoolIndex,
    },
    Jump {
        label_name_index: ConstantPoolIndex,
    },
    Return,
    Drop,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ConstantPoolIndex(pub u16);

impl ConstantPoolIndex {
    pub fn as_usize(self) -> usize {
        usize::from(self.0)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct LocalIndex(pub u16);

impl LocalIndex {
    pub fn as_usize(self) -> usize {
        usize::from(self.0)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Pointer(pub usize);

#[derive(Debug, Clone, Copy, PartialEq, Eq, TryFromPrimitive, IntoPrimitive)]
#[repr(u8)]
pub enum ConstantTag {
    Integer,
    Null,
    String,
    Method,
    Slot,
    Class,
    Boolean,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, TryFromPrimitive, IntoPrimitive)]
#[repr(u8)]
pub enum OpcodeTag {
    Label,
    Literal,
    Print,
    Array,
    Object,
    GetField,
    SetField,
    CallMethod,
    CallFunction,
    SetLocal,
    GetLocal,
    SetGlobal,
    GetGlobal,
    Branch,
    Jump,
    Return,
    Drop,
}
