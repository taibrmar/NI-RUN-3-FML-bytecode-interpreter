#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub fml); // load module synthesized by LALRPOP

#[macro_use]
mod debug; // Keep first so the macro is available everywhere

mod compiler;
mod data;
mod interpreter;
mod parser;

use std::fs;

use clap::Parser;

use crate::interpreter::Interpreter;

// TODO all the SPECs
//      They're places I've diverged from the spec because it was unclear or because I could get the same behavior with less effort.

// TODO all the LATERs
//      They're either things I'd do if I had infinite time
//      or edge cases I wanted to handle but I can plausibly say they didn't occur to me. ;)

fn main() {
    let cmd = Command::parse();

    match cmd {
        Command::Run {
            path_fml,
            heap_size,
            heap_log,
        } => {
            let text = fs::read_to_string(path_fml).unwrap();
            let bytes = compiler::compile(&text);

            let interpreter = Interpreter::new(bytes, heap_size, heap_log);
            interpreter.run();
        }
        Command::Compile { path_fml } => {
            let text = fs::read_to_string(&path_fml).unwrap();
            let bytecode = compiler::compile(&text);

            let path_bc = format!("{}.bc", path_fml.strip_suffix(".fml").unwrap()); // LATER accept -o flag
            fs::write(path_bc, bytecode).unwrap();
        }
        Command::Execute {
            path_bc,
            heap_size,
            heap_log,
        } => {
            let bytes =
                fs::read(&path_bc).expect(&format!("Couldn't load bytecode file at {}", &path_bc));
            let interpreter = Interpreter::new(bytes, heap_size, heap_log);
            interpreter.run();
        }
    }
}

#[derive(Debug, Parser)]
enum Command {
    Run {
        path_fml: String,

        #[clap(long, default_value = "1")]
        heap_size: usize,
        #[clap(long)]
        heap_log: Option<String>,
    },
    Compile {
        path_fml: String,
    },
    Execute {
        path_bc: String,

        #[clap(long, default_value = "1")]
        heap_size: usize,
        #[clap(long)]
        heap_log: Option<String>,
    },
}
