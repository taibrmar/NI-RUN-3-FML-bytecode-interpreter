use std::{
    collections::VecDeque,
    convert::{TryFrom, TryInto},
    fs::File,
    io::Write,
    mem,
    time::SystemTime,
};

use fnv::FnvHashMap;

use crate::data::{
    Class, Constant, ConstantPoolIndex, ConstantTag, LocalIndex, Method, Opcode, OpcodeTag, Pointer,
};

#[derive(Debug)]
pub struct Interpreter {
    const_pool: Vec<Constant>,
    #[allow(dead_code)]
    // This is split into global_functions and global_variables so it ends up unused
    globals: Vec<ConstantPoolIndex>,
    entry_point: ConstantPoolIndex,
    code: Vec<Opcode>,

    label_table: FnvHashMap<String, usize>,
    // SPEC Should be part of the global frame
    global_functions: FnvHashMap<String, ConstantPoolIndex>,
    // SPEC Value should be Pointer
    // SPEC This should technically be a frame on the frame stack
    global_variables: FnvHashMap<String, Value>,
    frame_stack: Vec<Frame>,
    operand_stack: Vec<Value>,
    // SPEC This should be one heap that also contains primitives
    array_heap: Vec<Array>,
    object_heap: Vec<Object>,

    heap_size_mb: usize,
    heap_log: Option<String>,
    heap_used_bytes: usize,
    heap_used_bytes_last: usize,
}

impl Interpreter {
    pub fn new(bytes: Vec<u8>, heap_size_mb: usize, heap_log: Option<String>) -> Self {
        let mut parser = Reader {
            cursor: &bytes[0..],
        };

        // Read constants (which include code)
        let mut const_pool = Vec::new();
        let mut code = Vec::new();
        let mut label_table = FnvHashMap::default();
        let const_pool_len = parser.read_u16();
        for _ in 0..const_pool_len {
            let const_tag = parser.read_u8();
            match const_tag.try_into().unwrap() {
                ConstantTag::Integer => {
                    let val = parser.read_i32();
                    const_pool.push(Constant::Integer(val));
                }
                ConstantTag::Null => const_pool.push(Constant::Null),
                ConstantTag::String => {
                    let len = parser.read_u32();
                    let s = parser.read_string(len);
                    const_pool.push(Constant::String(s));
                }
                ConstantTag::Method => {
                    let name_index = ConstantPoolIndex(parser.read_u16());
                    let num_arguments = parser.read_u8();
                    let num_locals = parser.read_u16();
                    let num_opcodes = parser.read_u32();
                    let start = code.len();

                    for _ in 0..num_opcodes {
                        let opcode_tag = parser.read_u8();
                        match opcode_tag.try_into().unwrap() {
                            OpcodeTag::Label => {
                                let label_name_index = ConstantPoolIndex(parser.read_u16());
                                let label_name = const_pool[label_name_index.as_usize()]
                                    .unwrap_str()
                                    .to_owned();
                                label_table.insert(label_name, code.len());
                                // There's no need to add the Label instruction to the code vector
                            }
                            OpcodeTag::Literal => {
                                let const_index = ConstantPoolIndex(parser.read_u16());
                                code.push(Opcode::Literal { const_index });
                            }
                            OpcodeTag::Print => {
                                let fmt_str_index = ConstantPoolIndex(parser.read_u16());
                                let num_args = parser.read_u8();
                                code.push(Opcode::Print {
                                    fmt_str_index,
                                    num_args,
                                });
                            }
                            OpcodeTag::Array => {
                                code.push(Opcode::Array);
                            }
                            OpcodeTag::Object => {
                                let class_index = ConstantPoolIndex(parser.read_u16());
                                code.push(Opcode::Object { class_index });
                            }
                            OpcodeTag::GetField => {
                                let name_index = ConstantPoolIndex(parser.read_u16());
                                code.push(Opcode::GetField { name_index });
                            }
                            OpcodeTag::SetField => {
                                let name_index = ConstantPoolIndex(parser.read_u16());
                                code.push(Opcode::SetField { name_index });
                            }
                            OpcodeTag::CallMethod => {
                                let name_index = ConstantPoolIndex(parser.read_u16());
                                let num_args = parser.read_u8();
                                code.push(Opcode::CallMethod {
                                    name_index,
                                    num_args,
                                });
                            }
                            OpcodeTag::CallFunction => {
                                let name_index = ConstantPoolIndex(parser.read_u16());
                                let num_args = parser.read_u8();
                                code.push(Opcode::CallFunction {
                                    name_index,
                                    num_args,
                                });
                            }
                            OpcodeTag::SetLocal => {
                                let local_index = LocalIndex(parser.read_u16());
                                code.push(Opcode::SetLocal { local_index });
                            }
                            OpcodeTag::GetLocal => {
                                let local_index = LocalIndex(parser.read_u16());
                                code.push(Opcode::GetLocal { local_index });
                            }
                            OpcodeTag::SetGlobal => {
                                let name_index = ConstantPoolIndex(parser.read_u16());
                                code.push(Opcode::SetGlobal { name_index });
                            }
                            OpcodeTag::GetGlobal => {
                                let name_index = ConstantPoolIndex(parser.read_u16());
                                code.push(Opcode::GetGlobal { name_index });
                            }
                            OpcodeTag::Branch => {
                                let label_name_index = ConstantPoolIndex(parser.read_u16());
                                code.push(Opcode::Branch { label_name_index });
                            }
                            OpcodeTag::Jump => {
                                let label_name_index = ConstantPoolIndex(parser.read_u16());
                                code.push(Opcode::Jump { label_name_index });
                            }
                            OpcodeTag::Return => {
                                code.push(Opcode::Return);
                            }
                            OpcodeTag::Drop => {
                                code.push(Opcode::Drop);
                            }
                        }
                    }

                    let length = code.len() - start;
                    let method = Method {
                        name_index,
                        num_arguments,
                        num_locals,
                        start,
                        length,
                    };
                    const_pool.push(Constant::Method(method));
                }
                ConstantTag::Slot => {
                    let const_index = ConstantPoolIndex(parser.read_u16());
                    const_pool.push(Constant::Slot(const_index));
                }
                ConstantTag::Class => {
                    let num_members = parser.read_u16();
                    let mut members = Vec::new();
                    for _ in 0..num_members {
                        let member_index = ConstantPoolIndex(parser.read_u16());
                        members.push(member_index);
                    }
                    let class = Class { members };
                    const_pool.push(Constant::Class(class));
                }
                ConstantTag::Boolean => {
                    // Really, Rust? No built-in way to do this?
                    let val = match parser.read_u8() {
                        0 => false,
                        1 => true,
                        _ => panic!("bad bool const"),
                    };
                    const_pool.push(Constant::Boolean(val));
                }
            }
        }

        // Read globals
        let mut globals = Vec::new();
        let mut global_variables = FnvHashMap::default();
        let mut global_functions = FnvHashMap::default();
        let globals_len = parser.read_u16();
        for _ in 0..globals_len {
            let global_index = ConstantPoolIndex(parser.read_u16());
            globals.push(global_index);
            match &const_pool[global_index.as_usize()] {
                Constant::Slot(name_index) => {
                    // Global variable
                    let name = const_pool[name_index.as_usize()].unwrap_str();
                    global_variables.insert(name.to_owned(), Value::Null);
                }
                Constant::Method(method) => {
                    // Global function (not an object's method)
                    let fn_name = const_pool[method.name_index.as_usize()].unwrap_str();
                    let prev = global_functions.insert(fn_name.to_owned(), global_index);
                    assert_eq!(prev, None);
                }
                x => panic!("Global must be a Slot or Method, got {:?}", x),
            }
        }

        // Read entry point
        // This seems to always be the last Method in the Constant Pool but I don't think it's guaranteed.
        let entry_point = ConstantPoolIndex(parser.read_u16());
        let entry_method = const_pool[entry_point.as_usize()].unwrap_method();
        assert_eq!(entry_method.num_arguments, 0);
        let entry_locals = vec![Value::Null; usize::from(entry_method.num_locals)];
        let frame = Frame {
            return_ip: usize::MAX,
            locals: entry_locals,
        };
        let frame_stack = vec![frame];

        Self {
            const_pool,
            globals,
            entry_point,
            code,

            label_table,
            global_functions,
            global_variables,
            frame_stack,
            operand_stack: Vec::new(),
            array_heap: Vec::new(),
            object_heap: Vec::new(),

            heap_size_mb,
            heap_log,
            heap_used_bytes: 0,
            heap_used_bytes_last: 0,
        }
    }

    pub fn run(mut self) {
        let mut log_file = self.heap_log.as_ref().map(|log_path| {
            let mut file = File::create(log_path).unwrap();
            writeln!(file, "timestamp,event,memory").unwrap();
            let timestamp = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_nanos();
            writeln!(file, "{},S,0", timestamp).unwrap();
            file
        });

        let entry_method = self.const_pool[self.entry_point.as_usize()].unwrap_method();
        let mut ip = entry_method.start;
        loop {
            // There's no return at the end of the implicit global function.
            // End the program when jumping out of the code vector.
            // Note that this needs to be at the start of the loop in case the function has 0 instructions.
            if ip >= self.code.len() {
                // Also, sanity check for the compiler:
                assert!(
                    self.operand_stack.len() <= 1,
                    "WARNING: stack not clean at exit: {:?}",
                    self.operand_stack
                );
                break;
            }

            let instr = self.code[ip];
            match instr {
                Opcode::Label { .. } => {
                    unreachable!("All labels should be removed when deserializing")
                }
                Opcode::Literal { const_index } => {
                    let value = match self.const_pool[const_index.as_usize()] {
                        Constant::Integer(val) => Value::Integer(val),
                        Constant::Boolean(val) => Value::Boolean(val),
                        Constant::Null => Value::Null,
                        Constant::String(_) => {
                            panic!("(probably) trying to push an invalid constant type")
                        }
                        Constant::Slot(_) => {
                            panic!("(probably) trying to push an invalid constant type")
                        }
                        Constant::Method(_) => {
                            panic!("(probably) trying to push an invalid constant type")
                        }
                        Constant::Class(_) => {
                            panic!("(probably) trying to push an invalid constant type")
                        }
                    };
                    self.operand_stack.push(value);
                    ip += 1;
                }
                Opcode::Print {
                    fmt_str_index,
                    num_args,
                } => {
                    let mut args = Vec::new();
                    for _ in 0..num_args {
                        args.push(self.operand_stack.pop().unwrap());
                    }
                    let mut args = args.into_iter().rev();

                    let format = self.const_pool[fmt_str_index.as_usize()].unwrap_str();
                    let mut res = String::new();
                    let mut escape = false;
                    for c in format.chars() {
                        if escape {
                            match c {
                                '"' => res.push('"'),
                                '\\' => res.push('\\'),
                                'n' => res.push('\n'),
                                't' => res.push('\t'),
                                '~' => res.push('~'),
                                c => panic!("Bad escape: {}", c),
                            }
                            escape = false;
                        } else {
                            match c {
                                '\\' => escape = true,
                                '~' => {
                                    let val = args.next().unwrap();
                                    res.push_str(&self.stringify(val));
                                }
                                c => res.push(c),
                            }
                        }
                    }
                    print!("{}", res);
                    self.operand_stack.push(Value::Null);
                    ip += 1;
                }
                Opcode::Array => {
                    if self.heap_used_bytes > self.heap_size_mb * 1_000_000 {
                        let timestamp = SystemTime::now()
                            .duration_since(SystemTime::UNIX_EPOCH)
                            .unwrap()
                            .as_nanos();

                        self.gc();

                        self.heap_used_bytes_last = self.heap_used_bytes;

                        if let Some(log_file) = &mut log_file {
                            writeln!(log_file, "{},G,{}", timestamp, self.heap_used_bytes).unwrap();
                        }
                    }

                    let initial_value = self.operand_stack.pop().unwrap();
                    let size = self.operand_stack.pop().unwrap();
                    let size = usize::try_from(size.unwrap_i32()).unwrap();

                    let pointer = Pointer(self.array_heap.len());
                    let array = Array(vec![initial_value; size]);
                    let alloc_size = array.size();
                    // LATER GC should be here (also see object)
                    self.heap_used_bytes += alloc_size;
                    self.array_heap.push(array);
                    self.operand_stack.push(Value::ArrayRef(pointer));

                    if let Some(log_file) = &mut log_file {
                        let now = self.heap_used_bytes as isize;
                        let last = self.heap_used_bytes_last as isize;
                        let diff = (now - last).abs();

                        if diff > 100_000 {
                            self.heap_used_bytes_last = self.heap_used_bytes;
                            let timestamp = SystemTime::now()
                                .duration_since(SystemTime::UNIX_EPOCH)
                                .unwrap()
                                .as_nanos();
                            writeln!(log_file, "{},A,{}", timestamp, self.heap_used_bytes).unwrap();
                        }
                    }

                    ip += 1;
                }
                Opcode::Object { class_index } => {
                    if self.heap_used_bytes > self.heap_size_mb * 1_000_000 {
                        let timestamp = SystemTime::now()
                            .duration_since(SystemTime::UNIX_EPOCH)
                            .unwrap()
                            .as_nanos();

                        self.gc();

                        self.heap_used_bytes_last = self.heap_used_bytes;

                        if let Some(log_file) = &mut log_file {
                            writeln!(log_file, "{},G,{}", timestamp, self.heap_used_bytes).unwrap();
                        }
                    }

                    let class = self.const_pool[class_index.as_usize()].unwrap_class();

                    let mut fields = FnvHashMap::default();
                    let mut methods = FnvHashMap::default();

                    for &member_index in class.members.iter().rev() {
                        match &self.const_pool[member_index.as_usize()] {
                            Constant::Slot(name_index) => {
                                let name = self.const_pool[name_index.as_usize()].unwrap_str();
                                let value = self.operand_stack.pop().unwrap();
                                fields.insert(name.to_owned(), value);
                            }
                            Constant::Method(method) => {
                                let name =
                                    self.const_pool[method.name_index.as_usize()].unwrap_str();
                                methods.insert(name.to_owned(), member_index);
                            }
                            _ => panic!("member must be slot or method"),
                        }
                    }

                    let parent = self.operand_stack.pop().unwrap();

                    let pointer = Pointer(self.object_heap.len());
                    let object = Object {
                        parent,
                        fields,
                        methods,
                    };
                    let alloc_size = object.size();
                    // LATER GC should be here (but need to pass parent and fields into gc() because they are roots)
                    self.heap_used_bytes += alloc_size;
                    self.object_heap.push(object);
                    self.operand_stack.push(Value::ObjectRef(pointer));

                    if let Some(log_file) = &mut log_file {
                        let now = self.heap_used_bytes as isize;
                        let last = self.heap_used_bytes_last as isize;
                        let diff = (now - last).abs();

                        if diff > 100_000 {
                            self.heap_used_bytes_last = self.heap_used_bytes;
                            let timestamp = SystemTime::now()
                                .duration_since(SystemTime::UNIX_EPOCH)
                                .unwrap()
                                .as_nanos();
                            writeln!(log_file, "{},A,{}", timestamp, self.heap_used_bytes).unwrap();
                        }
                    }

                    ip += 1;
                }
                Opcode::GetField { name_index } => {
                    let name = self.const_pool[name_index.as_usize()].unwrap_str();
                    let pointer = self.operand_stack.pop().unwrap().unwrap_object_ptr();
                    let object = &self.object_heap[pointer.0];
                    let value = object.fields.get(name).unwrap();
                    self.operand_stack.push(*value);
                    ip += 1;
                }
                Opcode::SetField { name_index } => {
                    let name = self.const_pool[name_index.as_usize()].unwrap_str();
                    let value = self.operand_stack.pop().unwrap();
                    let pointer = self.operand_stack.pop().unwrap().unwrap_object_ptr();
                    let object = &mut self.object_heap[pointer.0];
                    object.fields.insert(name.to_owned(), value).unwrap();
                    self.operand_stack.push(value);
                    ip += 1;
                }
                Opcode::CallMethod {
                    name_index,
                    num_args,
                } => {
                    let method_name = self.const_pool[name_index.as_usize()].unwrap_str();

                    let mut args = Vec::new();
                    for _ in 0..(num_args - 1) {
                        args.push(self.operand_stack.pop().unwrap());
                    }
                    args.reverse();
                    let mut receiver = self.operand_stack.pop().unwrap();

                    loop {
                        match receiver {
                            Value::Null => {
                                assert_eq!(args.len(), 1);
                                let rhs_val = args[0];
                                let res = if let Value::Null = rhs_val {
                                    match method_name {
                                        "==" | "eq" => Value::Boolean(true),
                                        "!=" | "neq" => Value::Boolean(false),
                                        _ => panic!(
                                            "Null (RHS Null) does not have method {}",
                                            method_name
                                        ),
                                    }
                                } else {
                                    match method_name {
                                        "==" | "eq" => Value::Boolean(false),
                                        "!=" | "neq" => Value::Boolean(true),
                                        _ => panic!(
                                            "Null (RHS other) does not have method {}",
                                            method_name
                                        ),
                                    }
                                };
                                self.operand_stack.push(res);
                                ip += 1;
                                break;
                            }
                            Value::Integer(l) => {
                                assert_eq!(args.len(), 1);
                                let rhs_val = args[0];
                                let res = if let Value::Integer(r) = rhs_val {
                                    match method_name {
                                        "+" | "add" => Value::Integer(l + r),
                                        "-" | "sub" => Value::Integer(l - r),
                                        "*" | "mul" => Value::Integer(l * r),
                                        "/" | "div" => Value::Integer(l / r),
                                        "%" | "mod" => Value::Integer(l % r),
                                        "<=" | "le" => Value::Boolean(l <= r),
                                        ">=" | "ge" => Value::Boolean(l >= r),
                                        "<" | "lt" => Value::Boolean(l < r),
                                        ">" | "gt" => Value::Boolean(l > r),
                                        "==" | "eq" => Value::Boolean(l == r),
                                        "!=" | "neq" => Value::Boolean(l != r),
                                        _ => panic!(
                                            "Integer (RHS Integer) does not have method {}",
                                            method_name
                                        ),
                                    }
                                } else {
                                    match method_name {
                                        "==" | "eq" => Value::Boolean(false),
                                        "!=" | "neq" => Value::Boolean(true),
                                        _ => panic!(
                                            "Integer (RHS other) does not have method {}",
                                            method_name
                                        ),
                                    }
                                };
                                self.operand_stack.push(res);
                                ip += 1;
                                break;
                            }
                            Value::Boolean(l) => {
                                assert_eq!(args.len(), 1);
                                let rhs_val = args[0];
                                let res = if let Value::Boolean(r) = rhs_val {
                                    match method_name {
                                        "&" | "and" => Value::Boolean(l & r),
                                        "|" | "or" => Value::Boolean(l | r),
                                        "==" | "eq" => Value::Boolean(l == r),
                                        "!=" | "neq" => Value::Boolean(l != r),
                                        _ => panic!(
                                            "Boolean (RHS Boolean) does not have method {}",
                                            method_name
                                        ),
                                    }
                                } else {
                                    match method_name {
                                        "==" | "eq" => Value::Boolean(false),
                                        "!=" | "neq" => Value::Boolean(true),
                                        _ => panic!(
                                            "Boolean (RHS other) does not have method {}",
                                            method_name
                                        ),
                                    }
                                };
                                self.operand_stack.push(res);
                                ip += 1;
                                break;
                            }
                            Value::ArrayRef(pointer) => {
                                let array = &mut self.array_heap[pointer.0];
                                let res = match method_name {
                                    "get" => {
                                        assert_eq!(args.len(), 1);
                                        let index = usize::try_from(args[0].unwrap_i32()).unwrap();
                                        array.0[index]
                                    }
                                    "set" => {
                                        assert_eq!(args.len(), 2);
                                        let value = args[1];
                                        let index = usize::try_from(args[0].unwrap_i32()).unwrap();

                                        let len = array.0.len(); // borrowck
                                        let elem = array.0.get_mut(index).expect(&format!(
                                            "IP: {}, index out of bounds - index: {} / len: {}",
                                            ip, index, len,
                                        ));
                                        *elem = value;

                                        value
                                    }
                                    _ => panic!("Array does not have method {}", method_name),
                                };
                                self.operand_stack.push(res);
                                ip += 1;
                                break;
                            }
                            Value::ObjectRef(pointer) => {
                                let object = &self.object_heap[pointer.0];
                                if let Some(method_index) = object.methods.get(method_name) {
                                    let method =
                                        self.const_pool[method_index.as_usize()].unwrap_method();

                                    assert_eq!(method.num_arguments, num_args);
                                    let num_args = usize::from(num_args);
                                    let num_locals = usize::from(method.num_locals);
                                    let mut locals = Vec::with_capacity(num_args + num_locals);
                                    locals.push(receiver);
                                    locals.extend(args);
                                    locals.resize(locals.len() + num_locals, Value::Null);
                                    let frame = Frame {
                                        return_ip: ip + 1,
                                        locals,
                                    };
                                    self.frame_stack.push(frame);
                                    ip = method.start;
                                    break;
                                }

                                receiver = object.parent;
                            }
                        }
                    }
                }
                Opcode::CallFunction {
                    name_index,
                    num_args,
                } => {
                    let fn_name = self.const_pool[name_index.as_usize()].unwrap_str();
                    let fn_index = self
                        .global_functions
                        .get(fn_name)
                        .expect(&format!("Can't find function: {}", fn_name));
                    let method = self.const_pool[fn_index.as_usize()].unwrap_method();

                    assert_eq!(method.num_arguments, num_args);
                    let num_args = usize::from(num_args);
                    let num_locals = usize::from(method.num_locals);

                    let mut locals = Vec::with_capacity(num_args + num_locals);
                    for _ in 0..num_args {
                        locals.push(self.operand_stack.pop().unwrap());
                    }
                    locals.reverse();
                    locals.resize(locals.len() + num_locals, Value::Null);
                    let frame = Frame {
                        return_ip: ip + 1,
                        locals,
                    };
                    self.frame_stack.push(frame);
                    ip = method.start;
                }
                Opcode::SetLocal { local_index } => {
                    let value = self.operand_stack.last().unwrap();
                    self.frame_stack.last_mut().unwrap().locals[local_index.as_usize()] = *value;
                    ip += 1;
                }
                Opcode::GetLocal { local_index } => {
                    let value = self.frame_stack.last().unwrap().locals[local_index.as_usize()];
                    self.operand_stack.push(value);
                    ip += 1;
                }
                Opcode::SetGlobal { name_index } => {
                    let name = self.const_pool[name_index.as_usize()].unwrap_str();
                    let value = self.operand_stack.last().unwrap();
                    let prev = self.global_variables.insert(name.to_owned(), *value);
                    assert!(prev.is_some(), "Undeclared global: {}", name);
                    ip += 1;
                }
                Opcode::GetGlobal { name_index } => {
                    let name = self.const_pool[name_index.as_usize()].unwrap_str();
                    let value = self
                        .global_variables
                        .get(name)
                        .expect(&format!("name {} not in global frame", name));
                    self.operand_stack.push(*value);
                    ip += 1;
                }
                Opcode::Branch { label_name_index } => {
                    let cond_value = self.operand_stack.pop().unwrap();
                    if cond_value.truthy() {
                        let label_name = self.const_pool[label_name_index.as_usize()].unwrap_str();
                        let label_dest = self
                            .label_table
                            .get(label_name)
                            .expect(&format!("label {} not found", label_name));
                        ip = *label_dest;
                    } else {
                        ip += 1;
                    }
                }
                Opcode::Jump { label_name_index } => {
                    let label_name = self.const_pool[label_name_index.as_usize()].unwrap_str();
                    let label_dest = self.label_table[label_name];
                    ip = label_dest;
                }
                Opcode::Return => {
                    let frame = self.frame_stack.pop().unwrap();

                    // If the last instruction in the code vector is a call,
                    // return will jump one past the one, hence the +1.
                    assert!(frame.return_ip < self.code.len() + 1);

                    ip = frame.return_ip;
                }
                Opcode::Drop => {
                    self.operand_stack.pop().unwrap();
                    ip += 1;
                }
            }
        }
    }

    fn gc(&mut self) {
        #[derive(Debug)]
        struct Gc {
            marks_array_heap: Vec<bool>,
            marks_object_heap: Vec<bool>,
            to_visit_arrays: VecDeque<usize>,
            to_visit_objects: VecDeque<usize>,
        }

        impl Gc {
            fn enque(&mut self, value: Value) {
                match value {
                    Value::ArrayRef(pointer) => {
                        if !self.marks_array_heap[pointer.0] {
                            self.to_visit_arrays.push_back(pointer.0);
                        }
                        self.marks_array_heap[pointer.0] = true;
                    }
                    Value::ObjectRef(pointer) => {
                        if !self.marks_object_heap[pointer.0] {
                            self.to_visit_objects.push_back(pointer.0);
                        }
                        self.marks_object_heap[pointer.0] = true;
                    }
                    _ => {}
                }
            }
        }

        let mut gc = Gc {
            marks_array_heap: vec![false; self.array_heap.len()],
            marks_object_heap: vec![false; self.object_heap.len()],
            to_visit_arrays: VecDeque::new(),
            to_visit_objects: VecDeque::new(),
        };

        // Mark roots
        for &value in self.global_variables.values() {
            gc.enque(value);
        }
        for frame in &self.frame_stack {
            for &local in &frame.locals {
                gc.enque(local);
            }
        }
        for &operand in &self.operand_stack {
            gc.enque(operand);
        }

        // Mark everything reachable using BFS
        loop {
            if let Some(array_index) = gc.to_visit_arrays.pop_front() {
                for &item in &self.array_heap[array_index].0 {
                    gc.enque(item);
                }
            } else if let Some(object_index) = gc.to_visit_objects.pop_front() {
                let object = &self.object_heap[object_index];
                gc.enque(object.parent);
                for &field in object.fields.values() {
                    gc.enque(field);
                }
            } else {
                break;
            }
        }

        // Nuke everything unreachable so it crashes if there's a bug
        // for array_index in 0..gc.marks_array_heap.len() {
        //     if !gc.marks_array_heap[array_index] {
        //         let array = &mut self.array_heap[array_index];
        //         array.0.clear();
        //     }
        // }
        // for object_index in 0..gc.marks_object_heap.len() {
        //     if !gc.marks_object_heap[object_index] {
        //         let object = &mut self.object_heap[object_index];
        //         object.parent = Value::Integer(1337);
        //         object.fields.clear();
        //         object.methods.clear();
        //     }
        // }

        // Sweep - compute forwarding pointers
        let mut forwarding_arrays = vec![0; self.array_heap.len()];
        let mut new_array_index = 0;
        for old_array_index in 0..gc.marks_array_heap.len() {
            if gc.marks_array_heap[old_array_index] {
                forwarding_arrays[old_array_index] = new_array_index;
                new_array_index += 1;
            }
        }
        let mut forwarding_objects = vec![0; self.object_heap.len()];
        let mut new_object_index = 0;
        for old_object_index in 0..gc.marks_object_heap.len() {
            if gc.marks_object_heap[old_object_index] {
                forwarding_objects[old_object_index] = new_object_index;
                new_object_index += 1;
            }
        }

        // Sweep - update pointers
        let rewrite_value = |value: &mut Value| match value {
            Value::ArrayRef(pointer) => {
                let new_array_index = forwarding_arrays[pointer.0];
                pointer.0 = new_array_index;
            }
            Value::ObjectRef(pointer) => {
                let new_object_index = forwarding_objects[pointer.0];
                pointer.0 = new_object_index;
            }
            _ => {}
        };

        for value in self.global_variables.values_mut() {
            rewrite_value(value)
        }
        for frame in &mut self.frame_stack {
            for local in &mut frame.locals {
                rewrite_value(local);
            }
        }
        for operand in &mut self.operand_stack {
            rewrite_value(operand);
        }
        for old_array_index in 0..gc.marks_array_heap.len() {
            if !gc.marks_array_heap[old_array_index] {
                continue;
            }

            let array = &mut self.array_heap[old_array_index];
            for item in &mut array.0 {
                rewrite_value(item);
            }
        }
        for old_object_index in 0..gc.marks_object_heap.len() {
            if !gc.marks_object_heap[old_object_index] {
                continue;
            }

            let object = &mut self.object_heap[old_object_index];
            rewrite_value(&mut object.parent);
            for field in object.fields.values_mut() {
                rewrite_value(field);
            }
        }

        // Sweep - compaction
        for old_array_index in 0..gc.marks_array_heap.len() {
            if !gc.marks_array_heap[old_array_index] {
                continue;
            }
            let new_array_index = forwarding_arrays[old_array_index];
            self.array_heap.swap(old_array_index, new_array_index);
        }
        for old_object_index in 0..gc.marks_object_heap.len() {
            if !gc.marks_object_heap[old_object_index] {
                continue;
            }
            let new_object_index = forwarding_objects[old_object_index];
            self.object_heap.swap(old_object_index, new_object_index);
        }
        self.array_heap.truncate(new_array_index);
        self.object_heap.truncate(new_object_index);

        // Recalculate size
        let mut size = 0;
        for array in &self.array_heap {
            size += array.size();
        }
        for object in &self.object_heap {
            size += object.size();
        }
        self.heap_used_bytes = size;
    }

    fn stringify(&self, value: Value) -> String {
        match value {
            Value::Integer(val) => val.to_string(),
            Value::Boolean(val) => val.to_string(),
            Value::Null => "null".to_owned(),
            Value::ArrayRef(pointer) => {
                let mut s = "[".to_owned();
                let mut first = true;
                let array = &self.array_heap[pointer.0];
                for value in &array.0 {
                    if !first {
                        s.push_str(", ");
                    }
                    first = false;
                    s.push_str(&self.stringify(*value));
                }
                s.push(']');
                s
            }
            Value::ObjectRef(pointer) => {
                let mut s = "object(".to_owned();
                let mut first = true;
                let object = &self.object_heap[pointer.0];

                if object.parent != Value::Null {
                    s.push_str("..=");
                    s.push_str(&self.stringify(object.parent));
                    first = false;
                }

                for (field, value) in &object.fields {
                    if !first {
                        s.push_str(", ");
                    }
                    first = false;
                    s.push_str(&field);
                    s.push('=');
                    s.push_str(&self.stringify(*value));
                }

                s.push(')');
                s
            }
        }
    }
}

#[derive(Debug, Clone)]
struct Frame {
    return_ip: usize,
    // SPEC These should also be pointers,
    locals: Vec<Value>,
}

// SPEC This should probably be called RuntimeObject
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Value {
    Integer(i32),
    Boolean(bool),
    Null,
    ArrayRef(Pointer),
    ObjectRef(Pointer),
}

impl Value {
    fn truthy(self) -> bool {
        match self {
            Self::Integer(_) => true,
            Self::Boolean(v) => v,
            Self::Null => false,
            Self::ArrayRef(_) => true,
            Self::ObjectRef(_) => true,
        }
    }

    fn unwrap_i32(&self) -> i32 {
        if let Value::Integer(value) = self {
            *value
        } else {
            panic!("{:?} is not an Integer", self)
        }
    }

    fn unwrap_object_ptr(&self) -> Pointer {
        if let Value::ObjectRef(pointer) = self {
            *pointer
        } else {
            panic!("{:?} is not an Object", self)
        }
    }
}

#[derive(Debug, Clone)]
struct Array(Vec<Value>);

impl Array {
    fn size(&self) -> usize {
        mem::size_of::<Array>() + self.0.len() * mem::size_of::<Value>()
    }
}

#[derive(Debug, Clone)]
struct Object {
    parent: Value,
    fields: FnvHashMap<String, Value>,
    methods: FnvHashMap<String, ConstantPoolIndex>,
}

impl Object {
    fn size(&self) -> usize {
        let header = mem::size_of::<Object>();
        let fields: usize = self
            .fields
            .iter()
            .map(|(name, _value)| name.len() + mem::size_of::<Value>())
            .sum();
        let methods: usize = self
            .methods
            .iter()
            .map(|(name, _index)| name.len() + mem::size_of::<ConstantPoolIndex>())
            .sum();
        header + fields + methods
    }
}

struct Reader<'a> {
    cursor: &'a [u8],
}

impl Reader<'_> {
    // Lol no generic T::from_le_bytes()

    fn read_i32(&mut self) -> i32 {
        let ret = i32::from_le_bytes(self.cursor[0..4].try_into().unwrap());
        self.cursor = &self.cursor[4..];
        ret
    }

    fn read_u8(&mut self) -> u8 {
        let ret = u8::from_le_bytes(self.cursor[0..1].try_into().unwrap());
        self.cursor = &self.cursor[1..];
        ret
    }

    fn read_u16(&mut self) -> u16 {
        let ret = u16::from_le_bytes(self.cursor[0..2].try_into().unwrap());
        self.cursor = &self.cursor[2..];
        ret
    }

    fn read_u32(&mut self) -> u32 {
        let ret = u32::from_le_bytes(self.cursor[0..4].try_into().unwrap());
        self.cursor = &self.cursor[4..];
        ret
    }

    fn read_string(&mut self, len: u32) -> String {
        let len = usize::try_from(len).unwrap();
        let ret = String::from_utf8(self.cursor[0..len].to_vec()).unwrap();
        self.cursor = &self.cursor[len..];
        ret
    }
}
