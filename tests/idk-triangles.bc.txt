Constant Pool:
0: "λ:"
1: "-"
2: "*"
3: 2
4: 1
5: 0
6: "loop:body:0"
7: "loop:condition:0"
8: "/"
9: "loop:body:1"
10: "loop:condition:1"
11: "if:consequent:2"
12: "if:end:2"
13: "|"
14: "<"
15: ">="
16: " "
17: "+"
18: "\n"
19: null
20: "triangle"
21: method #20 args:1 locals:4 0000-0064
22: 3
23: 5
24: method #0 args:0 locals:0 0065-0072
Entry: #24
Globals:
0: #21
Code:
0: lit #3
1: get local ::0
2: call slot #2 2
3: lit #4
4: call slot #1 2
5: set local ::1
6: drop
7: lit #5
8: set local ::2
9: drop
10: goto #7
11: label #6
12: get local ::1
13: lit #3
14: call slot #8 2
15: get local ::2
16: call slot #1 2
17: set local ::3
18: drop
19: lit #5
20: set local ::4
21: drop
22: goto #10
23: label #9
24: get local ::4
25: get local ::3
26: call slot #14 2
27: get local ::4
28: get local ::1
29: get local ::3
30: call slot #1 2
31: call slot #15 2
32: call slot #13 2
33: branch #11
34: printf #2 0
35: drop
36: goto #12
37: label #11
38: printf #16 0
39: drop
40: label #12
41: get local ::4
42: lit #4
43: call slot #17 2
44: set local ::4
45: drop
46: label #10
47: get local ::4
48: get local ::1
49: call slot #14 2
50: branch #9
51: printf #18 0
52: drop
53: get local ::2
54: lit #4
55: call slot #17 2
56: set local ::2
57: drop
58: label #7
59: get local ::2
60: get local ::0
61: call slot #14 2
62: branch #6
63: lit #19
64: return
65: lit #3
66: call #20 1
67: drop
68: lit #22
69: call #20 1
70: drop
71: lit #23
72: call #20 1

// >  * 
// > ***
// >   *  
// >  *** 
// > *****
// >     *    
// >    ***   
// >   *****  
// >  ******* 
// > *********
