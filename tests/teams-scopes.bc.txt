Constant Pool:
0: "λ:"
1: 5
2: "n"
3: slot #2
4: 8
5: "bar"
6: method #5 args:0 locals:0 0000-0001
7: "foo"
8: method #7 args:1 locals:1 0002-0006
9: "~\n"
10: 20
11: method #0 args:0 locals:1 0007-0015
Entry: #11
Globals:
0: #3
1: #6
2: #8
Code:
0: get global #2
1: return
2: get local ::0
3: set local ::1
4: drop
5: call #5 0
6: return
7: lit #1
8: set global #2
9: drop
10: lit #4
11: set local ::0
12: drop
13: lit #10
14: call #7 1
15: printf #9 1

// > 5
