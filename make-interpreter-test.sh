#!/bin/bash

# Regenerate all tests which have a .fml source using (some only have .bc/.bc.txt files):
# for file in tests/*.fml ; do ./make-test.sh $file ; done

set -euxo pipefail

cd ~/dev/rust/FML

name="$(dirname "$1")/$(basename "$1" .fml)"

cargo run -- parse ~/uni/run/du3-fml-bc/$name.fml -o ~/uni/run/du3-fml-bc/$name.json
cargo run -- compile ~/uni/run/du3-fml-bc/$name.json -o ~/uni/run/du3-fml-bc/$name.bc
cargo run -- disassemble ~/uni/run/du3-fml-bc/$name.bc > ~/uni/run/du3-fml-bc/$name.bc.txt
grep -e '// >' ~/uni/run/du3-fml-bc/$name.fml >> ~/uni/run/du3-fml-bc/$name.bc.txt

cd -
