#!/bin/bash

# Same dir as compiler but some tests only have a .bc file
tests_dir=tests

i=0
n=$(find "$tests_dir" -name '*.bc' | wc -l)

for test in "$tests_dir"/*.bc
do
  i=$((i + 1))
  filename="$(dirname "$test")/$(basename "$test" .bc)"
  outfile="$filename.out"
  difffile="$filename.diff"
  txtfile="$filename.bc.txt"

  message=$(echo -n "Executing test [$i/$n]: \"$test\"... ")
  echo -n "$message"

  message_length=$(echo -n "$message" | wc -c)
  for _ in $(seq 1 $((74 - $message_length)))
  do
    echo -n " "
  done

  ./fml-debug.sh execute "$test" 1> "$outfile" 2> "$outfile"

  diff <(grep -e '// >' < "$txtfile" | sed 's/\/\/ > \?//') "$outfile" > "$difffile"
  if test "$?" -eq 0
  then
    rm "$outfile"
    rm "$difffile"
    echo -e "\e[32mpassed\e[0m"
  else
    echo -e "\e[31mfailed\e[0m [details \"$difffile\"]"
  fi
done
