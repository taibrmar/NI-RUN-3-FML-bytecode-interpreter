# FML bytecode compiler and interpreter

Parser, some bash scripts and some tests are from https://github.com/kondziu/FML (slightly modified)

Usage:

```
cargo run -- compile file.fml  # will produce file.bc next to file.fml
cargo run -- execute file.bc
```

Or both steps at once:

```
cargo run -- run file.fml
```
